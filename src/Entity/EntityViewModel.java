package Entity;

import Entity.cards.Card;
import Entity.cards.Type;
import Entity.cards.minion.ArchmageArugal;
import Entity.cards.minion.Minion;
import Entity.cards.minion.SwampKingDred;
import Entity.cards.spell.ArcaneExplosion;
import Entity.cards.spell.Slam;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.BloodFury;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import Entity.hero.Mage;
import Entity.viewModel.*;
import com.sun.jdi.DoubleValue;

import java.util.ArrayList;
import java.util.List;

public class EntityViewModel {

    public DeckViewModel getDeck(Deck deck) {
        if (deck == null)
            return null;
        else {
            ArrayList<SpellViewModel> spellViewModels = new ArrayList<>();
            ArrayList<WeaponViewModel> weaponViewModels = new ArrayList<>();
            ArrayList<MinionViewModel> minionViewModels = new ArrayList<>();
            for (Card card : deck.getCards()) {
                if (card.getType().equals(Type.MINION))
                    minionViewModels.add(getMinion((Minion) card));
                else if (card.getType().equals(Type.SPELL))
                    spellViewModels.add(getSpell((Spell) card));
                else weaponViewModels.add(getWeapon((Weapon) card));
            }
            return new DeckViewModel(spellViewModels, weaponViewModels, minionViewModels, getHero(deck.getHero()), deck.getTexturePath(), deck.getName(),
                    deck.getNumberGame(), deck.getNumberWin());
        }
    }

    public SpellViewModel getSpell(Spell spell) {
        if (spell == null)
            return null;
        else {
            return new SpellViewModel(spell.getName(), spell.getCost(), spell.getMana(), spell.getRarity(), spell.getHeroClass(),
                    spell.getType(), spell.getDescription(), spell.getTexturePath(), spell.isLock(), spell.getNumberDraw());
        }
    }

    public CardViewModel getCard(Card card) {
        if (card == null)
            return null;
        else {
            return new CardViewModel(card.getName(), card.getCost(), card.getMana(), card.getRarity(), card.getHeroClass(),
                    card.getType(), card.getDescription(), card.getTexturePath(), card.isLock(), card.getNumberDraw());
        }
    }

    public MinionViewModel getMinion(Minion minion) {
        if (minion == null)
            return null;
        else {
            return new MinionViewModel(minion.getName(), minion.getCost(), minion.getMana(), minion.getRarity(), minion.getHeroClass(),
                    minion.getType(), minion.getDescription(), minion.getTexturePath(), minion.isLock(), minion.getNumberDraw(), minion.getHP(),
                    minion.getAttack(), minion.getMinionTexture());
        }
    }

    public WeaponViewModel getWeapon(Weapon card) {
        if (card == null)
            return null;
        else {
            return new WeaponViewModel(card.getName(), card.getCost(), card.getMana(), card.getRarity(), card.getHeroClass(),
                    card.getType(), card.getDescription(), card.getTexturePath(), card.isLock(), card.getNumberDraw(), card.getDurability(),
                    card.getAttack(), card.getWeaponTexture());
        }
    }

    public HeroViewModel getHero(Hero hero) {
        if (hero == null)
            return null;
        else {
            return new HeroViewModel(hero.getHeroClass(), hero.getHP(), hero.getMAX_HP(), hero.getTexturePath(), hero.getHeroTexture());
        }
    }

    public ArrayList<MinionViewModel> getMinions(ArrayList<Card> cards) {
        ArrayList<MinionViewModel> minionViewModels = new ArrayList<>();
        for (Card card : cards) {
            if (card.getType().equals(Type.MINION)) {
                minionViewModels.add(getMinion((Minion) card));
            }
        }
        return minionViewModels;
    }

    public ArrayList<SpellViewModel> getSpells(ArrayList<Card> cards) {
        ArrayList<SpellViewModel> spellViewModels = new ArrayList<>();
        for (Card card : cards) {
            if (card.getType().equals(Type.SPELL)) {
                spellViewModels.add(getSpell((Spell) card));
            }
        }
        return spellViewModels;
    }

    public ArrayList<WeaponViewModel> getWeapons(ArrayList<Card> cards) {
        ArrayList<WeaponViewModel> weaponViewModels = new ArrayList<>();
        for (Card card : cards) {
            if (card.getType().equals(Type.WEAPON))
                weaponViewModels.add(getWeapon((Weapon) card));
        }
        return weaponViewModels;
    }

    public List<HeroViewModel> getHeroes(List<Hero> heroes) {
        List<HeroViewModel> heroViewModels = new ArrayList<>();
        for (Hero hero : heroes) {
            heroViewModels.add(getHero(hero));
        }
        return heroViewModels;
    }

    public DeckViewModel[] getDecks(Deck[] decks) {
        DeckViewModel[] deckViewModels = new DeckViewModel[decks.length];
        for (int i = 0; i < decks.length; i++) {
            deckViewModels[i] = getDeck(decks[i]);
        }
        return deckViewModels;
    }

    public MinionViewModel[] getArrayMinions(Minion[] minions) {
        MinionViewModel[] minionViewModels = new MinionViewModel[minions.length];
        for (int i = 0; i < minions.length; i++) {
            minionViewModels[i] = getMinion(minions[i]);
        }
        return minionViewModels;
    }

    public Card setCard(CardViewModel cardViewModel) {
        if (cardViewModel == null)
            return null;
        else {
            return new Card(cardViewModel.getName(), cardViewModel.getCost(), cardViewModel.getMana(), cardViewModel.getRarity(),
                    cardViewModel.getHeroClass(), cardViewModel.getType(), cardViewModel.getDescription(), cardViewModel.getTexturePath(),
                    cardViewModel.isLock(), cardViewModel.getNumberDraw());
        }
    }

    public Hero setHero(HeroViewModel heroViewModel) {
        if (heroViewModel == null)
            return null;
        else {
            return new Mage(heroViewModel.getHeroClass(), MainPlayer.getInstance().getHeroes().get(0).getCards(),
                    heroViewModel.getTexturePath(), heroViewModel.getHeroTexture());
        }
    }

    public Deck setDeck(DeckViewModel deckViewModel) {
        if (deckViewModel == null)
            return null;
        else {
            List<Card> cards = new ArrayList<>();
            for (MinionViewModel minionViewModel : deckViewModel.getMinionViewModels())
                cards.add(setMinion(minionViewModel));
            for (SpellViewModel spellViewModel : deckViewModel.getSpellViewModels())
                cards.add(setSpell(spellViewModel));
            for (WeaponViewModel weaponViewModel : deckViewModel.getWeaponViewModels())
                cards.add(setWeapon(weaponViewModel));
            return new Deck(cards, setHero(deckViewModel.getHero()), deckViewModel.getTexturePath(), deckViewModel.getName(),
                    deckViewModel.getNumberGame(), deckViewModel.getNumberWin());
        }
    }

    public Minion setMinion(MinionViewModel minionViewModel) {
        if (minionViewModel == null)
            return null;
        else {
            return new SwampKingDred(minionViewModel.getName(), minionViewModel.getCost(), minionViewModel.getMana(), minionViewModel.getRarity(), minionViewModel.getHeroClass(),
                    minionViewModel.getType(), minionViewModel.getDescription(), minionViewModel.getTexturePath(), minionViewModel.isLock(), minionViewModel.getNumberDraw(), minionViewModel.getHP(), minionViewModel.getAttack());
        }
    }

    public Spell setSpell(SpellViewModel spellViewModel) {
        if (spellViewModel == null) {
            return null;
        } else {
            return new Slam(spellViewModel.getName(), spellViewModel.getCost(), spellViewModel.getMana(), spellViewModel.getRarity(),
                    spellViewModel.getHeroClass(), spellViewModel.getType(), spellViewModel.getDescription(), spellViewModel.getTexturePath(),
                    spellViewModel.isLock(), spellViewModel.getNumberDraw());
        }
    }

    public Weapon setWeapon(WeaponViewModel weaponViewModel) {
        if (weaponViewModel == null) {
            return null;
        } else {
            return new BloodFury(weaponViewModel.getName(), weaponViewModel.getCost(), weaponViewModel.getMana(), weaponViewModel.getRarity(),
                    weaponViewModel.getHeroClass(), weaponViewModel.getType(), weaponViewModel.getDescription(), weaponViewModel.getTexturePath(),
                    weaponViewModel.isLock(), weaponViewModel.getNumberDraw(), weaponViewModel.getDurability(), weaponViewModel.getAttack());
        }
    }


}
