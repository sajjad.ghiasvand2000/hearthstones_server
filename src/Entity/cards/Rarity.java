package Entity.cards;

import java.io.Serializable;

public enum Rarity implements Serializable {
    COMMON,
    RARE,
    EPIC,
    LEGENDARY
}
