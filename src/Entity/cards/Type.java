package Entity.cards;

import java.io.Serializable;

public enum Type implements Serializable {
    SPELL,
    MINION,
    WEAPON,
    MISSION
}
