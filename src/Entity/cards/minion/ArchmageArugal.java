package Entity.cards.minion;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.Util;
import states.playState.logic.GamePlayerLogic;

public class ArchmageArugal extends Battlecry {
    public ArchmageArugal(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }



    @Override
    public void battlecry(GamePlayerLogic GPL) {
        GPL.addACardToDeck(Util.findCardFromMainPlayer("Archmage Arugal"));
    }
}
