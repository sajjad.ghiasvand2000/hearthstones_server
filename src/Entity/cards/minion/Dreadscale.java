package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.play.MainMapper;
import states.playState.logic.GamePlayerLogic;

public class Dreadscale extends EndTurn {
    public Dreadscale(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void endTurn(GamePlayerLogic GPL) {
        if (GPL.getName().equals("player1")) {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL2(), 1);
        } else {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL1(), 1);
        }
    }

}
