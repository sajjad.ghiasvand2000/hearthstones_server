package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class GadgetzanAuctioneer extends Minion {
    public GadgetzanAuctioneer(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public void castSpellThenDrawACard(GamePlayerLogic GPL){
        EntityUtils.addACardFromDeckToHand(GPL);
    }
}
