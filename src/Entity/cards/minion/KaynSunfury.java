package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.play.MainMapper;
import states.playState.logic.GamePlayerLogic;

public class KaynSunfury extends Battlecry {
    public KaynSunfury(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void battlecry(GamePlayerLogic GPL) {
       charge = true;
       if (GPL.getName().equals("player1")){
           EntityUtils.ignoreAllEnemyTaunt(MainMapper.getGPL2());
       }else {
           EntityUtils.ignoreAllEnemyTaunt(MainMapper.getGPL1());
       }
    }
}
