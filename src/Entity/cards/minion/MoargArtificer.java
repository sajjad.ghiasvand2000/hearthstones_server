package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.play.MainMapper;
import states.playState.logic.GamePlayerLogic;

public class MoargArtificer extends Minion {
    public MoargArtificer(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public void doubleDamage(GamePlayerLogic GPL){
        if (GPL.getName().equals("player1")){
            EntityUtils.makeAllMinionDoubleDamage(MainMapper.getGPL2());
        }else
            EntityUtils.makeAllMinionDoubleDamage(MainMapper.getGPL1());
    }
}
