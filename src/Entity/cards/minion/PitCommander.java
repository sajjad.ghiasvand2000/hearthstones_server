package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Card;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class PitCommander extends EndTurn {
    public PitCommander(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void endTurn(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        Card card = EntityUtils.getAMinionFromDeck(GPL);
        if (x != -1 && card != null) {
            GPL.setOneContainLandCard(card, x);
            GPL.setNumberOfCards(GPL.getNumberOfCards() - 1);
        }
    }
}
