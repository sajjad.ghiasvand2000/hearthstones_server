package Entity.cards.minion;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class Sathrovarr extends Battlecry{
    public Sathrovarr(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    @Override
    public void battlecry(GamePlayerLogic GPL) {
        Minion minion1 = GPL.getMinionLogic().getSathrovarr().factory2();
        Minion minion2 = GPL.getMinionLogic().getSathrovarr().factory2();
        Minion minion3 = GPL.getMinionLogic().getSathrovarr().factory2();
        GPL.addACardToDeck(minion1);
        GPL.getDeck().addCard(minion2);
        GPL.setNumberOfCards(GPL.getNumberOfCards() + 1);
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        if (x != -1){
            GPL.setOneContainLandCard(minion3, x);
        }
    }
}
