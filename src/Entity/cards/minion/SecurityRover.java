package Entity.cards.minion;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;

public class SecurityRover extends Minion {
    public SecurityRover(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public SecurityRover(SecurityRover securityRover){
        super(securityRover);
    }
}
