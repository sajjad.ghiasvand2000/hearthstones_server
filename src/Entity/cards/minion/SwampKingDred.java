package Entity.cards.minion;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;

public class SwampKingDred extends Minion {
    public SwampKingDred(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, HP, attack, texturePath, lock);
    }

    public SwampKingDred(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock, int numberDraw, Integer HP, Integer attack) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock, numberDraw, HP, attack);
    }
}
