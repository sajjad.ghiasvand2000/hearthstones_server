package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.play.MainMapper;
import states.playState.logic.GamePlayerLogic;

public class ArcaneExplosion extends SingleSpell {
    public ArcaneExplosion(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        if (GPL.getName().equals("player1")) {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL2(), 1);
        } else {
            EntityUtils.decreaseHPFromAllMinion(MainMapper.getGPL1(), 1);
        }
    }
}
