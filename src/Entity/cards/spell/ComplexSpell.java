package Entity.cards.spell;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public abstract class ComplexSpell extends Spell {
    public ComplexSpell(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    public abstract void complexSpell(GamePlayerLogic GPL, Minion minion);
}
