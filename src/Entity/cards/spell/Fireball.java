package Entity.cards.spell;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class Fireball extends ComplexSpell {
    public Fireball(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void complexSpell(GamePlayerLogic GPL, Minion minion) {
        if (minion.isShield()){
            minion.setShield(false);
        }else {
            if (minion.isDoubleDamage())
            minion.setHP(minion.getHP() - 12);
            else minion.setHP(minion.getHP() - 6);
            GPL.setUpdateFlagACardInLand(GPL.getMinionLogic().getIndexMinionWantAttack());
        }
    }
}
