package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.RotnestDrake;
import Entity.hero.HeroClass;
import main.Util;
import states.playState.logic.GamePlayerLogic;

public class LearnDraconic extends QuestReward {
    public LearnDraconic(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void start(GamePlayerLogic GPL) {
       GPL.getSpellLogic().setSpellQuest(0);
    }

    @Override
    public boolean quest(GamePlayerLogic GPL) {
        return GPL.getSpellLogic().getSpellQuest() >= 8;
    }

    @Override
    public void reward(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        if (x != -1){
            RotnestDrake rotnestDrake = new RotnestDrake((RotnestDrake) Util.findCardFromMainPlayer("Rotnest Drake"));
            rotnestDrake.setHP(6);
            GPL.setOneContainLandCard(rotnestDrake, x);
        }
    }

    @Override
    public double developmentPercent(GamePlayerLogic GPL) {
        return GPL.getSpellLogic().getSpellQuest()/8.0;
    }
}
