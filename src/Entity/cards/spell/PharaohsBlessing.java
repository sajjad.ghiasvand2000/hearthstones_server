package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class PharaohsBlessing extends SingleSpell{

    public PharaohsBlessing(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        int x = EntityUtils.finedNonZeroRandomIndex(GPL);
        if (x != -1) {
            Minion minion = GPL.getContainLandCard()[x];
            minion.setHP(minion.getHP() + 4);
            minion.setAttack(minion.getAttack() + 4);
            minion.setShield(true);
            minion.setTaunt(true);
            GPL.setUpdateFlagACardInLand(x);
        }
    }


}
