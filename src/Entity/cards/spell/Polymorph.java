package Entity.cards.spell;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.cards.minion.SecurityRover;
import Entity.hero.HeroClass;
import main.Util;
import states.playState.logic.GamePlayerLogic;

public class Polymorph extends ComplexSpell {
    public Polymorph(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void complexSpell(GamePlayerLogic GPL, Minion minion) {
        SecurityRover securityRover = new SecurityRover((SecurityRover) Util.findCardFromMainPlayer("Security Rover"));
        securityRover.setHP(1);
        securityRover.setAttack(1);
        GPL.setOneContainLandCard(securityRover, GPL.getMinionLogic().getIndexMinionWantAttack());
    }
}
