package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class SenseDemons extends SingleSpell {
    public SenseDemons(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        EntityUtils.addACardFromDeckToHand(GPL);
        EntityUtils.addACardFromDeckToHand(GPL);
    }
}
