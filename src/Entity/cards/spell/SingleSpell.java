package Entity.cards.spell;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public abstract class SingleSpell extends Spell{
    public SingleSpell(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    public SingleSpell(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock, int numberDraw) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock, numberDraw);
    }

    public abstract void singleSpell(GamePlayerLogic GPL);
}
