package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.hero.HeroClass;
import main.play.MainMapper;
import states.playState.logic.GamePlayerLogic;

public class Slam extends SingleSpell {
    public Slam(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    public Slam(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock, int numberDraw) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock, numberDraw);
    }

    @Override
    public void singleSpell(GamePlayerLogic GPL) {
        Minion minion;
        if (GPL.getName().equals("player1")){
            int x = EntityUtils.finedNonZeroRandomIndex(MainMapper.getGPL2());
            if (x != -1) {
                minion = MainMapper.getGPL2().getContainLandCard()[x];
                minion.setHP(minion.getHP() - 2);
                if (minion.getHP() > 0){
                    EntityUtils.addACardFromDeckToHand(GPL);
                }
                MainMapper.getGPL2().setUpdateFlagACardInLand(x);
            }
        }else {
            int x = EntityUtils.finedNonZeroRandomIndex(MainMapper.getGPL1());
            if (x != -1) {
                minion = MainMapper.getGPL1().getContainLandCard()[x];
                minion.setHP(minion.getHP() - 2);
                if (minion.getHP() > 0){
                    EntityUtils.addACardFromDeckToHand(GPL);
                }
                MainMapper.getGPL1().setUpdateFlagACardInLand(x);
            }

        }
    }
}
