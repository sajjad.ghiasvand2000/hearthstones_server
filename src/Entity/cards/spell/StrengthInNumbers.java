package Entity.cards.spell;

import Entity.EntityUtils;
import Entity.cards.Card;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import main.Util;
import states.playState.logic.GamePlayerLogic;

public class StrengthInNumbers extends QuestReward {
    public StrengthInNumbers(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, questAndReward, texturePath, lock);
    }

    @Override
    public void start(GamePlayerLogic GPL) {
        GPL.getMinionLogic().setMinionQuest(0);
    }

    @Override
    public boolean quest(GamePlayerLogic GPL) {
        return GPL.getMinionLogic().getMinionQuest() >= 10;
    }

    @Override
    public void reward(GamePlayerLogic GPL) {
        int x = EntityUtils.finedZeroRandomIndex(GPL);
        Card card;
        if (EntityUtils.getReward().equals(""))
            card = EntityUtils.getAMinionFromDeck(GPL);
        else card = Util.findCardFromMainPlayer(EntityUtils.getReward());
        if (x != -1 && card != null) {
            GPL.setOneContainLandCard(card, x);
            GPL.setNumberOfCards(GPL.getNumberOfCards() - 1);
        }
    }

    @Override
    public double developmentPercent(GamePlayerLogic GPL) {
        return GPL.getMinionLogic().getMinionQuest()/10.0;
    }

}
