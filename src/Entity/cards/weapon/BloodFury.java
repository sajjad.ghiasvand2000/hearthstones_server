package Entity.cards.weapon;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;

public class BloodFury extends Weapon {
    public BloodFury(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
    }

    public BloodFury(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock, int numberDraw, Integer durability, Integer attack) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock, numberDraw, durability, attack);
    }
}
