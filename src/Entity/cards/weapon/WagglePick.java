package Entity.cards.weapon;

import Entity.EntityUtils;
import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;
import states.playState.logic.GamePlayerLogic;

public class WagglePick extends Deathrattle {
    public WagglePick(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
    }

    @Override
    public void deathrattle(GamePlayerLogic GPL) {
        EntityUtils.addACardFromDeckToHand(GPL);
    }
}
