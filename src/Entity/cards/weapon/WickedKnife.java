package Entity.cards.weapon;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;

public class WickedKnife extends Weapon {
    public WickedKnife(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, durability, attack, texturePath, lock);
    }
}
