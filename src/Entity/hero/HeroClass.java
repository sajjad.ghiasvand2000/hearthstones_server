package Entity.hero;

import java.io.Serializable;

public enum HeroClass implements Serializable {
    Mage,
    Rogue,
    Warlock,
    NATURAL,
    Hunter,
    Paladin
}
