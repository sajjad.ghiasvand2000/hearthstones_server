package Entity.hero;

import Entity.cards.Card;
import states.playState.logic.GamePlayerLogic;

import java.util.List;

public class Mage extends Hero {

    public Mage() {
    }

    public Mage(HeroClass heroClass, List<Card> cards, String[] texturePath, String[] heroTexture) {
        super(heroClass, cards, texturePath, heroTexture);
    }

    public Mage(Hero hero){
        super(hero);
        HP = 30;
        MAX_HP = 30;
    }

    @Override
    public void heroPower(GamePlayerLogic GPL) {
        GPL.getHeroLogic().setHeroPowerWantAttack(true);
    }

}
