package Entity.viewModel;

import java.io.Serializable;
import java.util.ArrayList;

public class DeckDeckViewModel implements Serializable {

    private ArrayList<CardViewModel> cardViewModels;
    private HeroViewModel hero;
    private String texturePath;
    private String name;
    private int numberGame;
    private int numberWin;

    public DeckDeckViewModel(ArrayList<CardViewModel> cardViewModels, HeroViewModel hero, String texturePath, String name, int numberGame, int numberWin) {
        this.cardViewModels = cardViewModels;
        this.hero = hero;
        this.texturePath = texturePath;
        this.name = name;
        this.numberGame = numberGame;
        this.numberWin = numberWin;
    }

    public ArrayList<CardViewModel> getCardViewModels() {
        return cardViewModels;
    }

    public void setCardViewModels(ArrayList<CardViewModel> cardViewModels) {
        this.cardViewModels = cardViewModels;
    }

    public HeroViewModel getHero() {
        return hero;
    }

    public void setHero(HeroViewModel hero) {
        this.hero = hero;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberGame() {
        return numberGame;
    }

    public void setNumberGame(int numberGame) {
        this.numberGame = numberGame;
    }

    public int getNumberWin() {
        return numberWin;
    }

    public void setNumberWin(int numberWin) {
        this.numberWin = numberWin;
    }
}