package Entity.viewModel;

import Entity.cards.minion.Minion;
import Entity.cards.weapon.Weapon;
import Entity.hero.HeroClass;

import java.io.Serializable;
import java.util.List;

public class HeroViewModel implements Serializable {

    private HeroClass heroClass;
    private int HP;
    private int MAX_HP;
    private String[] texturePath;
    private String[] heroTexture;

    public HeroViewModel(HeroClass heroClass, int HP, int MAX_HP, String[] texturePath, String[] heroTexture) {
        this.heroClass = heroClass;
        this.HP = HP;
        this.MAX_HP = MAX_HP;
        this.texturePath = texturePath;
        this.heroTexture = heroTexture;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public int getHP() {
        return HP;
    }

    public void setHP(int HP) {
        this.HP = HP;
    }

    public int getMAX_HP() {
        return MAX_HP;
    }

    public void setMAX_HP(int MAX_HP) {
        this.MAX_HP = MAX_HP;
    }

    public String[] getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }

    public String[] getHeroTexture() {
        return heroTexture;
    }

    public void setHeroTexture(String[] heroTexture) {
        this.heroTexture = heroTexture;
    }
}
