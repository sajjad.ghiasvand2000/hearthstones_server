package Entity.viewModel;

import Entity.cards.Rarity;
import Entity.cards.Type;
import Entity.hero.HeroClass;

import java.io.Serializable;

public class WeaponViewModel implements Serializable {
    private String name;
    private int cost;
    private int mana;
    private Rarity rarity;
    private HeroClass heroClass;
    private Type type;
    private String description;
    private String[] texturePath;
    private boolean lock;
    private int numberDraw;
    private Integer durability;
    private Integer attack;
    private String[] WeaponTexture;

    public WeaponViewModel(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock, int numberDraw, Integer durability, Integer attack, String[] weaponTexture) {
        this.name = name;
        this.cost = cost;
        this.mana = mana;
        this.rarity = rarity;
        this.heroClass = heroClass;
        this.type = type;
        this.description = description;
        this.texturePath = texturePath;
        this.lock = lock;
        this.numberDraw = numberDraw;
        this.durability = durability;
        this.attack = attack;
        WeaponTexture = weaponTexture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String[] getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public int getNumberDraw() {
        return numberDraw;
    }

    public void setNumberDraw(int numberDraw) {
        this.numberDraw = numberDraw;
    }

    public Integer getDurability() {
        return durability;
    }

    public void setDurability(Integer durability) {
        this.durability = durability;
    }

    public Integer getAttack() {
        return attack;
    }

    public void setAttack(Integer attack) {
        this.attack = attack;
    }

    public String[] getWeaponTexture() {
        return WeaponTexture;
    }

    public void setWeaponTexture(String[] weaponTexture) {
        WeaponTexture = weaponTexture;
    }
}
