package data;

import Entity.Player;
import com.google.gson.Gson;
import Entity.MainPlayer;

import java.io.*;
import java.util.Map;
import java.util.Scanner;

public class MyFile {

    public static void fileWriter(String fileName, Object object) throws IOException {
        String data = new Gson().toJson(object);
        FileWriter fileWriter = new FileWriter(fileName, true);
        fileWriter.write(data + "\r\n");
        fileWriter.flush();
        fileWriter.close();
    }

    public static void deletePlayer(String fileName, String username) throws IOException {
        FileWriter fileWriter = new FileWriter("player1.txt");
        Scanner Reader = new Scanner(new FileReader(fileName));
        while (Reader.hasNext()) {
            String data = Reader.nextLine();
            Gson gson = new Gson();
            Map map = gson.fromJson(data, Map.class);
            if (username.equals(map.get("username"))) continue;
            else {
                fileWriter.write(data + "\r\n");
            }
            fileWriter.flush();
        }
        Reader.close();
        fileWriter.close();
        File file1 = new File("player1.txt");
        File file = new File(fileName);
        file.delete();
        file1.renameTo(file);
    }

    public static boolean isRepetitiveUsername(String username, String fileName) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new FileReader(fileName));
        while (fileReader.hasNext()) {
            Gson gson = new Gson();
            Map map = gson.fromJson(fileReader.nextLine(), Map.class);
            if (map != null) {
                if (map.get("username").equals(username)) {
                    fileReader.close();
                    return true;
                }
            }
        }
        fileReader.close();
        return false;
    }

    public static void saveChanges(Player player) {
        try {
            MyFile.deletePlayer("player.txt", player.getUsername());
            MyFile.fileWriter("player.txt", player);
        }catch (IOException e){
            e.getStackTrace();
        }
    }

}
