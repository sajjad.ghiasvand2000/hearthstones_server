package login;

import Entity.Player;

import java.io.IOException;

public class LoginMapper {

    public Player registerState(String username, String password) throws IOException {
        return new RegisterState(username, password).getPlayer();
    }

    public Player loginState(String username, String password) throws IOException {
        return new LoginState(username, password).getPlayer();
    }
}
