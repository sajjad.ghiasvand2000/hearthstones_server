package login;

import Entity.Deck;
import Entity.Player;
import Entity.cards.Card;
import Entity.cards.minion.Minion;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.Weapon;
import com.google.gson.Gson;
import Entity.MainPlayer;
import com.google.gson.internal.LinkedTreeMap;
import data.Log;
import data.Setter;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Scanner;

public class LoginState {
    private String username;
    private String password;
    private String result;
    private Player player;

    public LoginState(String username, String password) throws IOException {
        this.username = username;
        this.password = password;
        player = new Player();
        result = init();
    }

    private String init() throws IOException {
        Scanner fileReader = new Scanner(new FileReader("player.txt"));
        while (fileReader.hasNext()) {
            Gson gson = new Gson();
            Map map = gson.fromJson(fileReader.nextLine(), Map.class);
            if (username.equals(map.get("username")) && password.equals(map.get("password"))) {
                ArrayList<Card> entireCards = new ArrayList<>();
                ArrayList<Deck> allDecks = new ArrayList<>();
                int[] levels = new int[10];
                Double userId = (Double) map.get("userId");
                Double diamond = (Double) map.get("diamond");
                Double cup = (Double) map.get("cup");
                ArrayList cards = (ArrayList) map.get("entireCards");
                ArrayList decks = (ArrayList) map.get("decks");
                ArrayList level = (ArrayList)map.get("level");
                for (int i = 0; i < level.size(); i++) {
                    levels[i] = ((Double)level.get(i)).intValue();
                }
                for (int i = 0; i < 17; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    Minion minion = (Minion) Minion.factory(linkedTreeMap);
                    entireCards.add(minion);
                }
                for (int i = 17; i < 32; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    entireCards.add((Spell) Spell.factory(linkedTreeMap));
                }
                for (int i = 32; i < 37; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) cards.get(i);
                    entireCards.add((Weapon) Weapon.factory(linkedTreeMap));
                }
                player.setUserId(userId.intValue());
                MainPlayer.getInstance().setUserId(userId.intValue());
                player.setUsername((String) map.get("username"));
                MainPlayer.getInstance().setUsername((String) map.get("username"));
                player.setPassword((String) map.get("password"));
                MainPlayer.getInstance().setPassword((String) map.get("password"));
                player.setHeroes(Setter.setHero());
                MainPlayer.getInstance().setHeroes(Setter.setHero());
                for (int i = 0; i < 13; i++) {
                    LinkedTreeMap linkedTreeMap = (LinkedTreeMap) decks.get(i);
                    if (linkedTreeMap != null) {
                        allDecks.add((Deck) Deck.factory(linkedTreeMap));
                        System.out.println(1);
                    }
                    else allDecks.add(null);
                }
                player.setEntireCards(entireCards);
                MainPlayer.getInstance().setEntireCards(entireCards);
                player.setDecks(allDecks.toArray(new Deck[13]));
                MainPlayer.getInstance().setDecks(allDecks.toArray(new Deck[13]));
                player.setDiamond(diamond.intValue());
                MainPlayer.getInstance().setDiamond(diamond.intValue());
                player.setCup(cup.intValue());
                MainPlayer.getInstance().setCup(cup.intValue());
                player.setLevel(levels);
                MainPlayer.getInstance().setLevel(levels);
                Log.body(player, "login", "successful login");
                fileReader.close();
                return "pass";
            }
        }
        fileReader.close();
        return "fail";
    }

    public String getResult() {
        return result;
    }

    public Player getPlayer() {
        return player;
    }
}
