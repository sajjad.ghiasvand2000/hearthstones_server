package login;

import Entity.Player;
import Entity.hero.Hero;
import Entity.cards.Card;
import Entity.MainPlayer;
import Entity.Deck;
import data.Log;
import data.MyFile;
import data.Setter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegisterState {
    private String username;
    private String password;
    private String result;
    private Player player;

    public RegisterState(String username, String password) throws IOException {
        this.username = username;
        this.password = password;
        player = new Player();
        result = init();
    }

    private String init() throws IOException {
        if (MyFile.isRepetitiveUsername(username, "player.txt")) {
            return "fail";
        }
        int numberId = Log.userId();
        List<Hero> heroes = Setter.setHero();
        ArrayList<Card> cards = Setter.setCard();
        player.setUserId(numberId);
        MainPlayer.getInstance().setUserId(numberId);
        player.setUsername(username);
        MainPlayer.getInstance().setUsername(username);
        player.setPassword(password);
        MainPlayer.getInstance().setPassword(password);
        player.setHeroes(heroes);
        MainPlayer.getInstance().setHeroes(heroes);
        player.setEntireCards(cards);
        MainPlayer.getInstance().setEntireCards(cards);
        player.setDiamond(50);
        MainPlayer.getInstance().setDiamond(50);
        player.setDecks(new Deck[13]);
        MainPlayer.getInstance().setDecks(new Deck[13]);
        MyFile.fileWriter("player.txt", player);
        Log.createLog(numberId, username, password);
        return "pass";
    }

    public String getResult() {
        return result;
    }

    public Player getPlayer() {
        return player;
    }
}
