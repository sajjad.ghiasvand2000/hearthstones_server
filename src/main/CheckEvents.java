package main;

import states.playState.MapperPlay;

public class CheckEvents extends Thread {
    private MapperPlay mapperPlay;
    private ClientHandler clientHandler;

    public CheckEvents(MapperPlay mapperPlay, ClientHandler clientHandler){
        this.clientHandler = clientHandler;
        this.mapperPlay = mapperPlay;
    }
    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        while (true) {
            if (mapperPlay.getUpdateFlagACardInLand() != -1) {
                clientHandler.updateHP(mapperPlay.getUpdateFlagACardInLand(), mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.changeUpdateFlagACardInLand();
            }

            if (mapperPlay.isSpecialUpdateFlag()) {
                clientHandler.specialUpdate(mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.changeSpecialUpdateFlag();
            }

            if (mapperPlay.isUpdateFlagDeck()) {
                clientHandler.cardDeckInit(mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.makeFalseUpdateFlagDeck();
            }

            if (mapperPlay.isUpdateFlagDeck()) {
                clientHandler.cardDeckInit(mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.makeFalseUpdateFlagDeck();
            }

            if (mapperPlay.getExtraCard() != null) {
                clientHandler.cardDeckInitFirst(mapperPlay);
            }

            if (mapperPlay.isHeroUpdateFlag()) {
                clientHandler.updateHero(mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.changeHeroUpdateFlag();
            }

            if (mapperPlay.isUpdateWeaponFlag()) {
                clientHandler.updateWeapon(mapperPlay);
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mapperPlay.changeUpdateWeaponFlag();
            }

            if (!mapperPlay.isQuestRewardNull()) {
                if (mapperPlay.quest()) {
                    mapperPlay.reward();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mapperPlay.changeQuestReward();
                    clientHandler.done(mapperPlay);
                }
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
