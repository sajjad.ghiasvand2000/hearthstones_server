package main;

import main.play.Play;

public class CheckNetStatus extends Thread {
    private ClientHandler p1;
    private ClientHandler p2;
    private Play play;

    public CheckNetStatus(ClientHandler p1, ClientHandler p2, Play play) {
        this.p1 = p1;
        this.p2 = p2;
        this.play = play;
    }

    @Override
    public void run() {
        while (true){
            if (p1.getSocket().isClosed() && !p1.isExitInPlay()){
                boolean flag = false;
                p2.poorEnemyConnection("Your enemy is unconnected");
                for (int i = 0; i < 30; i++) {
                    if (p1.getSocket().isConnected()){
                        flag = true;
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!flag){
                    play.setEndGame1(true);
                    break;
                }else {
                    p2.poorEnemyConnection("null");
                }
            }else if (p2.getSocket().isClosed() && !p2.isExitInPlay()){
                boolean flag = false;
                p1.poorEnemyConnection("Your enemy is unconnected");
                for (int i = 0; i < 30; i++) {
                    if (p2.getSocket().isConnected()){
                        flag = true;
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!flag){
                    play.setEndGame2(true);
                    break;
                }else {
                    p1.poorEnemyConnection("null");
                }
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
