package main;

import Entity.Deck;
import Entity.EntityViewModel;
import Entity.Player;
import Entity.cards.Card;
import Entity.cards.Type;
import Entity.cards.minion.Minion;
import Entity.cards.spell.Spell;
import Entity.cards.weapon.Weapon;
import Entity.hero.Hero;
import Entity.viewModel.*;
import configs.*;
import data.DataMapper;
import data.MyFile;
import login.LoginMapper;
import main.play.Play;
import main.play.Training;
import states.collectionState.CollectionStateLogic;
import states.collectionState.DeckReader;
import states.playState.MapperPlay;
import states.rankState.RankLogic;
import states.statusState.MapperStatus;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientHandler extends Thread {
    private String name;
    private Socket socket;
    private Socket socket1;
    private Server server;
    private Player player;
    private DataInputStream dataInputStream;
    private DataInputStream dataInputStream1;
    private DataOutputStream dataOutputStream;
    private DataOutputStream dataOutputStream1;
    private ObjectOutputStream objectOutputStream;
    private ObjectOutputStream objectOutputStream1;
    private ObjectInputStream objectInputStream;
    private LoginMapper loginMapper;
    private DataMapper dataMapper;
    private EntityViewModel entityViewModel;
    private CollectionStateConfigs c = CollectionStateConfigs.getInstance();
    private PassiveStateConfigs p = PassiveStateConfigs.getInstance();
    private PlayStateConfigs play = PlayStateConfigs.getInstance();
    private MenuStateConfigs m = MenuStateConfigs.getInstance();
    private ShopStateConfigs s = ShopStateConfigs.getInstance();
    private CollectionStateLogic collectionStateLogic;
    private MapperStatus mapperStatus;
    private MapperPlay mapperPlay;
    private MapperPlay enemyMapperPlay;
    private String clientState = "start";
    private Play PLAY;
    private Deck currentDeck;
    private boolean startFromMenu;
    private boolean ready;
    private boolean training;
    private Training train;
    private boolean exitInPlay;
    private boolean stop;

    public ClientHandler(Socket socket, Socket socket1, Server server) throws IOException {
        this.socket = socket;
        this.socket1 = socket1;
        this.server = server;
        dataOutputStream = new DataOutputStream(socket.getOutputStream());
        dataOutputStream1 = new DataOutputStream(socket1.getOutputStream());
        objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
        objectOutputStream1 = new ObjectOutputStream(socket1.getOutputStream());
        dataInputStream = new DataInputStream(socket.getInputStream());
        dataInputStream1 = new DataInputStream(socket1.getInputStream());
        objectInputStream = new ObjectInputStream(socket.getInputStream());
        dataMapper = new DataMapper();
        loginMapper = new LoginMapper();
        entityViewModel = new EntityViewModel();
        listen();
    }

    @Override
    public void run() {
        while (true) {
            try {
                String request = dataInputStream.readUTF();
                String[] data = request.split("_");
                switch (data[0]) {
                    case "register":
                        Player player1 = loginMapper.registerState(data[1], data[2]);
                        if (player1.getHeroes() != null) {
                            player = player1;
                            dataOutputStream.writeUTF("pass");
                            dataOutputStream.flush();
                        } else {
                            dataOutputStream.writeUTF("fail");
                            dataOutputStream.flush();
                        }
                        break;
                    case "login":
                        Player player2 = loginMapper.loginState(data[1], data[2]);
                        if (player2.getHeroes() != null) {
                            player = player2;
                            dataOutputStream.writeUTF("pass");
                            dataOutputStream.flush();
                        } else {
                            dataOutputStream.writeUTF("fail");
                            dataOutputStream.flush();
                        }
                        break;
                    case "delete":
                        if (MyFile.isRepetitiveUsername(data[1], "player.txt")) {
                            MyFile.deletePlayer("player.txt", data[1]);
                            dataOutputStream.writeUTF("pass");
                            dataOutputStream.flush();
                        } else {
                            dataOutputStream.writeUTF("fail");
                            dataOutputStream.flush();
                        }
                        break;
                    case "log":
                        if (data[1].equals("hi")) {
                            startFromMenu = true;
                        } else dataMapper.log(player, data[1], data[2]);
                        break;
                    case "save":
                        MyFile.saveChanges(player);
                        break;
                    case "name":
                        dataOutputStream.writeUTF(name);
                        dataOutputStream.flush();
                        break;
                    case "passive":
                        mapperPlay.setTwiceDrawPassive(Boolean.parseBoolean(data[1]));
                        mapperPlay.setOffCardPassive(Integer.parseInt(data[2]));
                        mapperPlay.setManaJumpPassive(Integer.parseInt(data[3]));
                        mapperPlay.setFreePower(Integer.parseInt(data[4]));
                        mapperPlay.setNurse(Boolean.parseBoolean(data[5]));
                        break;
                    case "massage":
                        if (name.equals("player1"))
                            PLAY.getGamePlayer2().massage(data[1]);
                        else PLAY.getGamePlayer1().massage(data[1]);
                        break;
                    case "startPlay":
                        if (data[1].equals("usual")) {
                            clientState = "wait";
                            training = false;
                        } else if (data[1].equals("deckReader")) {
                            clientState = "deckReader";
                            training = false;
                        } else {
                            clientState = "training";
                            training = true;
                            startTraining();
                        }
                        break;
                    case "ready":
                        ready = true;
                        break;
                    case "changeGamePlayer":
                        if (!training)
                            PLAY.changeGamePlayer();
                        else train.changeGamePlayer();
                        break;
                    case "endTurnClicked":
                        if (!training) {
                            if (name.equals("player1"))
                                PLAY.getGamePlayer2().endTurnClicked();
                            else PLAY.getGamePlayer1().endTurnClicked();
                        }
                        break;
                    case "summonMinion":
                        if (!training) {
                            if (name.equals("player1"))
                                PLAY.getGamePlayer2().summonMinion(Integer.parseInt(data[1]));
                            else PLAY.getGamePlayer1().summonMinion(Integer.parseInt(data[1]));
                        }
                        break;
                    case "collection":
                        switch (data[1]) {
                            case "deck":
                                currentDeck = new Deck(entityViewModel.setDeck((DeckViewModel) objectInputStream.readObject()));
                                break;
                        }
                        break;
                    case "statusState":
                        mapperStatus = new MapperStatus(player);
                        ArrayList<String> decks = new ArrayList<>();
                        for (Deck comparedDeck : mapperStatus.getComparedDecks()) {
                            decks.add(comparedDeck.toString());
                        }
                        objectOutputStream.writeObject(decks);
                        break;
                    case "rankState":
                        RankLogic rankLogic = new RankLogic(player);
                        objectOutputStream.writeObject(rankLogic.findTopTenScores());
                        objectOutputStream.flush();
                        objectOutputStream.writeObject(rankLogic.findMe());
                        objectOutputStream.flush();
                        break;
                    case "exitInPlay":
                        exitInPlay = true;
                        deleteClientHandler();
                        break;
                    case "changeCard":
                        switch (data[1]) {
                            case "deck":
                                objectOutputStream.writeObject(entityViewModel.getDeck(collectionStateLogic.getPlayDeck()));
                                objectOutputStream.flush();
                                break;
                        }
                        break;
                    case "PLAYER1":
                    case "PLAYER2":
                        play(enemyMapperPlay, data);
                        break;
                    case "player1":
                    case "player2":
                        play(mapperPlay, data);
                        break;
                    case "config":
                        switch (data[1]) {
                            case "collection":
                                dataOutputStream.writeUTF(Integer.toString(c.get(data[2])));
                                dataOutputStream.flush();
                                break;
                            case "play":
                                dataOutputStream.writeUTF(Integer.toString(play.get(data[2])));
                                dataOutputStream.flush();
                                break;
                            case "passive":
                                dataOutputStream.writeUTF(Integer.toString(p.get(data[2])));
                                dataOutputStream.flush();
                                break;
                            case "menu":
                                dataOutputStream.writeUTF(Integer.toString(m.getMenuSateConfigs(data[2])));
                                dataOutputStream.flush();
                                break;
                            case "shop":
                                dataOutputStream.writeUTF(Integer.toString(s.get(data[2])));
                                dataOutputStream.flush();
                                break;
                        }
                        break;
                    case "MainPlayer":
                        switch (data[1]) {
                            case "deck":
                                objectOutputStream.writeObject(entityViewModel.getDeck(player.getDecks()[Integer.parseInt(data[2])]));
                                objectOutputStream.flush();
                                break;
                            case "setDeck":
                                Deck deck = new Deck(entityViewModel.setDeck((DeckViewModel) objectInputStream.readObject()));
                                player.setDeck(deck, Integer.parseInt(data[2]));
                                break;
                            case "entireCards":
                                objectOutputStream.writeObject(entityViewModel.getMinions(player.getEntireCards()));
                                objectOutputStream.flush();
                                objectOutputStream.writeObject(entityViewModel.getSpells(player.getEntireCards()));
                                objectOutputStream.flush();
                                objectOutputStream.writeObject(entityViewModel.getWeapons(player.getEntireCards()));
                                objectOutputStream.flush();
                                break;
                            case "userId":
                                dataOutputStream.writeUTF(Integer.toString(player.getUserId()));
                                dataOutputStream.flush();
                                break;
                            case "heroes":
                                objectOutputStream.writeObject(entityViewModel.getHeroes(player.getHeroes()));
                                objectOutputStream.flush();
                                break;
                            case "decks":
                                objectOutputStream.writeObject(entityViewModel.getDecks(player.getDecks()));
                                objectOutputStream.flush();
                                break;
                            case "diamond":
                                dataOutputStream.writeUTF(Integer.toString(player.getDiamond()));
                                dataOutputStream.flush();
                                break;
                            case "setDiamond":
                                player.setDiamond(Integer.parseInt(data[2]));
                                break;
                        }
                        break;
                }
            } catch (IOException | ClassNotFoundException e) {
                deleteClientHandler();
                if (PLAY != null)
                    connectionProblem();
                System.out.println("client disconnected");
                break;
            }
        }
    }

    private void connectionProblem() {
        if (!exitInPlay) {
            if (player.getUsername().equals(PLAY.getGamePlayer1().player.getUsername())) {
                boolean flag = false;
                PLAY.getGamePlayer2().poorEnemyConnection("Your enemy is unconnected");
                for (int i = 0; i < 10; i++) {
//            if (p1.getSocket().isConnected()){
//                flag = true;
//                break;
//            }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!flag) {
                    PLAY.setEndGame1(true);
                } else {
                    PLAY.getGamePlayer2().poorEnemyConnection("null");
                }
            } else {
                boolean flag = false;
                PLAY.getGamePlayer1().poorEnemyConnection("Your enemy is unconnected");
                for (int i = 0; i < 10; i++) {
//                    if (p2.getSocket().isConnected()){
//                        flag = true;
//                        break;
//                    }
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (!flag) {
                    PLAY.setEndGame2(true);
                } else {
                    PLAY.getGamePlayer1().poorEnemyConnection("null");
                }
            }
        }
    }

    public synchronized void play(MapperPlay mapperPlay, String[] data) throws IOException, ClassNotFoundException {
        if (data[1].equals("play")) {
            switch (data[2]) {
                case "clickEndTurn":
                    mapperPlay.clickEndTurn();
                    break;
                case "addCardToHand":
                    mapperPlay.addCardToHand();
                    break;
                case "clickDeckCard":
                    switch (data[3]) {
                        case "spell":
                            mapperPlay.clickDeckCard(entityViewModel.setSpell((SpellViewModel) objectInputStream.readObject()).factory2());
                            break;
                        case "minion":
                            mapperPlay.clickDeckCard(entityViewModel.setMinion((MinionViewModel) objectInputStream.readObject()).factory2());
                            break;
                        case "weapon":
                            mapperPlay.clickDeckCard(entityViewModel.setWeapon((WeaponViewModel) objectInputStream.readObject()).factory2());
                            break;
                    }
                    break;
                case "getDeck":
                    objectOutputStream.writeObject(entityViewModel.getDeck(mapperPlay.getDeck()));
                    objectOutputStream.flush();
                    break;
                case "getMinionWantedSummon":
                    objectOutputStream.writeObject(entityViewModel.getMinion(mapperPlay.getMinionWantedSummon()));
                    objectOutputStream.flush();
                    break;
                case "makeMinionsAttackable":
                    mapperPlay.makeMinionsAttackable();
                    break;
                case "heroPower":
                    mapperPlay.heroPower();
                    break;
                case "getIndexMinionWantAttack":
                    dataOutputStream.writeUTF(Integer.toString(mapperPlay.getIndexMinionWantAttack()));
                    dataOutputStream.flush();
                    break;
                case "getDeckCardSize":
                    dataOutputStream.writeUTF(Integer.toString(mapperPlay.getDeckCardSize()));
                    dataOutputStream.flush();
                    break;
                case "changeUpdateFlagDeck":
                    mapperPlay.makeTrueUpdateFlagDeck();
                    break;
                case "changeExtraCard":
                    mapperPlay.changeExtraCard();
                    break;
                case "clickOnHero":
                    mapperPlay.clickOnHero();
                    break;
                case "changeErrorMassage":
                    mapperPlay.changeErrorMassage();
                    break;
                case "weaponAttack":
                    mapperPlay.weaponAttack();
                    break;
                case "checkDurability":
                    mapperPlay.checkDurability();
                    break;
                case "checkHPLandCards":
                    mapperPlay.checkHPLandCards(Integer.parseInt(data[3]));
                    break;
                case "getHero":
                    objectOutputStream.writeObject(entityViewModel.getHero(mapperPlay.getHero()));
                    objectOutputStream.flush();
                    break;
                case "clickLandCard":
                    mapperPlay.clickLandCard(Integer.parseInt(data[3]));
                    break;
                case "getEvent":
                    dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "getEvent");
                    dataOutputStream1.flush();
                    objectOutputStream1.writeObject(mapperPlay.getEvent());
                    objectOutputStream1.flush();
                    break;
                case "removeDeckMinion":
                    mapperPlay.removeDeckMinion(entityViewModel.setMinion((MinionViewModel) objectInputStream.readObject()).factory2(), Integer.parseInt(data[3]));
                    break;
                case "getContainLandCard":
                    objectOutputStream.writeObject(entityViewModel.getArrayMinions(mapperPlay.getContainLandCard()));
                    objectOutputStream.flush();
                    break;
                case "getWeapon":
                    objectOutputStream.writeObject(entityViewModel.getWeapon(mapperPlay.getWeapon()));
                    objectOutputStream.flush();
                    break;
                case "getHeroClass":
                    objectOutputStream.writeObject(mapperPlay.getHeroClass());
                    objectOutputStream.flush();
                    break;
                case "changeError":
                    mapperPlay.changeError();
                    break;
                case "getExtraCard":
                    helper(mapperPlay.getExtraCard());
                    break;
                case "getDeckCard":
                    helper(mapperPlay.getDeckCard(Integer.parseInt(data[3])));
                    break;
                case "getContainDeckCard":
                    objectOutputStream.writeObject(entityViewModel.getMinions(mapperPlay.getContainDeckCard()));
                    objectOutputStream.flush();
                    objectOutputStream.writeObject(entityViewModel.getSpells(mapperPlay.getContainDeckCard()));
                    objectOutputStream.flush();
                    objectOutputStream.writeObject(entityViewModel.getWeapons(mapperPlay.getContainDeckCard()));
                    objectOutputStream.flush();
                    break;
                case "isTurn":
                    dataOutputStream.writeUTF(Boolean.toString(mapperPlay.isTurn()));
                    dataOutputStream.flush();
                    break;
            }
        }
    }

    private void helper(Card extraCard) throws IOException {
        if (extraCard == null) {
            dataOutputStream.writeUTF("null");
            dataOutputStream.flush();
        } else {
            if (extraCard.getType().equals(Type.WEAPON)) {
                dataOutputStream.writeUTF("weapon");
                dataOutputStream.flush();
                objectOutputStream.writeObject(entityViewModel.getWeapon((Weapon) extraCard));
                objectOutputStream.flush();
            } else if (extraCard.getType().equals(Type.SPELL)) {
                dataOutputStream.writeUTF("spell");
                dataOutputStream.flush();
                objectOutputStream.writeObject(entityViewModel.getSpell((Spell) extraCard));
                objectOutputStream.flush();
            } else if (extraCard.getType().equals(Type.MINION)) {
                dataOutputStream.writeUTF("minion");
                dataOutputStream.flush();
                objectOutputStream.writeObject(entityViewModel.getMinion((Minion) extraCard));
                objectOutputStream.flush();
            } else {
                dataOutputStream.writeUTF("null");
                dataOutputStream.flush();
            }
        }
    }

    private void startFromMenu() {
        stop = false;
        collectionStateLogic = new CollectionStateLogic(player);
        mapperPlay = new MapperPlay(name, collectionStateLogic.getPlayDeck(), player);
    }

    private void startFromCollection() {
        stop = false;
        collectionStateLogic = new CollectionStateLogic(player);
        collectionStateLogic.setPlayDeck(currentDeck);
        mapperPlay = new MapperPlay(name, currentDeck, player);
    }

    private void startTraining() {
        stop = false;
        name = "player1";
        startFromCollection();
        setPlayState();
        train = new Training(this);
        train.start();
        new CheckEvents(enemyMapperPlay, this).start();
        new CheckEvents(mapperPlay, this).start();
    }

    public void startFromDeckReader(DeckReader deckReader) {
        collectionStateLogic = new CollectionStateLogic(player);
        if (name.equals("player1")) {
            collectionStateLogic.setPlayDeck(deckReader.getDeck1());
            mapperPlay = new MapperPlay(name, deckReader.getDeck1(), player);
        } else {
            collectionStateLogic.setPlayDeck(deckReader.getDeck2());
            mapperPlay = new MapperPlay(name, deckReader.getDeck2(), player);
        }
    }

    public void startGame() {
        if (startFromMenu) {
            startFromMenu();
            startFromMenu = false;
        } else startFromCollection();
    }

    public Socket getSocket() {
        return socket;
    }

    public String getClientState() {
        return clientState;
    }

    public void setClientState(String clientState) {
        this.clientState = clientState;
    }

    public void setPlayState() {
        clientState = "play";
        try {
            dataOutputStream.writeUTF("play started");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MapperPlay getMapperPlay() {
        return mapperPlay;
    }

    public void setClientName(String name) {
        this.name = name;
    }

    public String getClientName() {
        return name;
    }

    public void setPLAY(Play PLAY) {
        this.PLAY = PLAY;
    }

    public void setEnemyMapperPlay(MapperPlay enemyMapperPlay) {
        this.enemyMapperPlay = enemyMapperPlay;
    }

    public MapperPlay getEnemyMapperPlay() {
        return enemyMapperPlay;
    }

    public Player getPlayer() {
        return player;
    }


    public boolean isReady() {
        return ready;
    }

    public boolean isExitInPlay() {
        return exitInPlay;
    }

    public synchronized void minionAttack() {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "minionAttack");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateHP(int i, MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "updateHP_" + i);
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateHero(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "updateHero");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateWeapon(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "updateWeapon");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void enemyMinionAttack() {
        try {
            dataOutputStream1.writeUTF(enemyMapperPlay.getName() + "_" + "minionAttack");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public synchronized void specialUpdate(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "specialUpdate");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void cardDeckInit(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "cardDeckInit");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void cardDeckInitFirst(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "cardDeckInitFirst");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void done(MapperPlay mapperPlay) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "done");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void massage(String i) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "massage_" + i);
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void endTurnClicked() {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "endTurnClicked");
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void summonMinion(int i) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "summonMinion_" + i);
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void endGame(String s) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "endGame_" + s);
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public synchronized void poorEnemyConnection(String s) {
        try {
            dataOutputStream1.writeUTF(mapperPlay.getName() + "_" + "poorEnemyConnection_" + s);
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteClientHandler(){
        for (int i = 0; i < server.getClients().size(); i++) {
            if (server.getClients().get(i).player.getUsername().equals(player.getUsername())){
                server.getClients().remove(server.getClients().get(i));
            }
        }
    }

    public synchronized void updateParameter(MapperPlay mapperPlay1) {
        try {
            dataOutputStream1.writeUTF(mapperPlay1.getName() + "_" + "updateParameter_" + mapperPlay1.getMana() + "_" +
                    mapperPlay1.getLevel() + "_" + mapperPlay1.getNumberOfCards() + "_" + mapperPlay1.isQuestRewardNull() +
                    "_" + mapperPlay1.developmentPercent() + "_" + mapperPlay1.getErrorMassage() + "_" + mapperPlay1.isError());
            dataOutputStream1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void listen(){
        new Thread(() -> {
            while (true) {
                if (!stop) {
                    String request;
                    try {
                        MapperPlay mapperPlay1;
                        request = dataInputStream1.readUTF();
                        String[] data = request.split("_");
                        if (data[0].equals(name)) mapperPlay1 = mapperPlay;
                        else mapperPlay1 = enemyMapperPlay;
                        updateParameter(mapperPlay1);
                    } catch (IOException e) {
                        System.out.println();
                        break;
                    }
                }
            }
        }).start();
    }

    public void setStop(boolean stop) {
        this.stop = stop;
    }
}
