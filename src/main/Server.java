package main;

import main.play.Play;
import states.collectionState.DeckReader;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server extends Thread {
    private ArrayList<ClientHandler> clients;
    private ServerSocket serverSocket;

    Server(int serverPort) throws IOException {
        serverSocket = new ServerSocket(serverPort);
        clients = new ArrayList<>();
        new Thread(() -> {
            try {
                checkForPlaying();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }).start();
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                Socket socket = serverSocket.accept();
                Socket socket1 = serverSocket.accept();
                ClientHandler clientHandler = new ClientHandler(socket, socket1, this);
                clients.add(clientHandler);
                System.out.println("Client at: " + socket.getRemoteSocketAddress().toString() + " is connected.");
                clientHandler.start();
                Thread.sleep(500);
            } catch (IOException | InterruptedException e) {
                System.out.println("socket closed");
            }
        }
    }

    private void checkForPlaying() throws IOException {
        while (true) {
            if (clients.size() > 1) {
                for (int i = 0; i < clients.size(); i++) {
                    for (int j = i + 1; j < clients.size(); j++) {
                        if (clients.get(i).getClientState().equals("wait") && clients.get(j).getClientState().equals("wait")) {
                            if (Util.checkWMA(clients.get(i).getPlayer(), clients.get(j).getPlayer())) {
                                clients.get(i).setClientName("player1");
                                clients.get(j).setClientName("player2");
                                clients.get(i).startGame();
                                clients.get(j).startGame();
                                startPlaying(clients.get(i), clients.get(j));
                            }
                        } else if (clients.get(i).getClientState().equals("deckReader") && clients.get(j).getClientState().equals("deckReader")) {
                            if (Util.checkWMA(clients.get(i).getPlayer(), clients.get(j).getPlayer())) {
                                clients.get(i).setClientName("player1");
                                clients.get(j).setClientName("player2");
                                DeckReader deckReader = new DeckReader();
                                clients.get(i).startFromDeckReader(deckReader);
                                clients.get(j).startFromDeckReader(deckReader);
                                startPlaying(clients.get(i), clients.get(j));
                            }
                        }
                    }
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void startPlaying(ClientHandler client1, ClientHandler client2) {
        client1.setPlayState();
        client2.setPlayState();
        new Thread(() -> {
            while (true) {
                if (client1.isReady() && client2.isReady()) {
                    client1.setPlayState();
                    client2.setPlayState();
                    break;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        Play play = new Play(client1, client2);
        client1.setEnemyMapperPlay(client2.getMapperPlay());
        client2.setEnemyMapperPlay(client1.getMapperPlay());
        new CheckEvents(client1.getEnemyMapperPlay(), client1).start();
        new CheckEvents(client1.getMapperPlay(), client1).start();
        new CheckEvents(client2.getEnemyMapperPlay(), client2).start();
        new CheckEvents(client2.getMapperPlay(), client2).start();
        client1.setPLAY(play);
        client2.setPLAY(play);
        play.start();
        System.out.println("client " + client1.getSocket().getRemoteSocketAddress() + " is going to play with "
                + client2.getSocket().getRemoteSocketAddress());
    }

    public ArrayList<ClientHandler> getClients() {
        return clients;
    }
}