package main;

import configs.ConfigLoader;

import java.io.IOException;

public class ServerMain {
    static int serverPort = 8000;

    public static void main(String[] args) throws IOException {
        new ServerMain(args);
    }

    private ServerMain(String[] args) throws IOException {
        ConfigLoader.getInstance(getConfigFile(args));
        new Server(serverPort).start();
    }
    private String getConfigFile(String[] args){
        String configAddress = "default";
        if(args.length > 1){
            configAddress = args[1];
        }
        return configAddress;
    }
}