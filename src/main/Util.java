package main;

import Entity.Player;
import Entity.cards.Card;
import Entity.MainPlayer;
import Entity.hero.Hero;
import Entity.hero.HeroClass;

public class Util {

    public static int searchMainPlayerCards(Card card) {
        for (int i = 0; i < MainPlayer.getInstance().getEntireCards().size(); i++) {
            Card entireCard = MainPlayer.getInstance().getEntireCards().get(i);
            if (entireCard.equals(card))
                return i;
        }
        return -1;
    }

    public static Hero finedHero(HeroClass heroClass){
        for (Hero hero : MainPlayer.getInstance().getHeroes()) {
            if (hero.getHeroClass().toString().equals(heroClass.toString())){
                return hero;
            }
        }
        return null;
    }

    public static Card findCardFromMainPlayer(String card){
        for (Card entireCard : MainPlayer.getInstance().getEntireCards()) {
            if (entireCard.getName().equals(card))
                return entireCard;
        }
        return null;
    }


    private static Double weightedMovingAverage(int[] results){
        int total = 0;
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            total += results[i]*(10 - i);
            sum += 10 - i;
        }
        return total*1.0/sum;
    }

    public static boolean checkWMA(Player p1, Player p2){
        return Math.abs(weightedMovingAverage(p1.getLevel()) - weightedMovingAverage(p2.getLevel())) < 0.3;
    }
}
