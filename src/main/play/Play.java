package main.play;

import Entity.Deck;
import Entity.Player;
import Entity.cards.minion.Minion;
import data.Log;
import data.MyFile;
import main.CheckNetStatus;
import main.ClientHandler;
import states.playState.MapperPlay;

public class Play extends Thread {
    private static MainMapper mainMapper;
    private ClientHandler gamePlayer1;
    private ClientHandler gamePlayer2;
    private ClientHandler currentGamePlayer;
    private MapperPlay map1;
    private MapperPlay map2;
    private Minion minion1;
    private Minion minion2;
    private boolean endGame1;
    private boolean endGame2;

    public Play(ClientHandler gamePlayer1, ClientHandler gamePlayer2) {
        this.gamePlayer1 = gamePlayer1;
        this.gamePlayer2 = gamePlayer2;
        map1 = gamePlayer1.getMapperPlay();
        map2 = gamePlayer2.getMapperPlay();
        MainMapper.setMainMapper(new MainMapper(map1.getGamePlayerLogic(), map2.getGamePlayerLogic()));
        mainMapper = MainMapper.getInstance();
        mainMapper.setFirstTurn();
        currentGamePlayer = gamePlayer1;
        new CheckNetStatus(gamePlayer1, gamePlayer2, this).start();
    }

    @Override
    public void run() {
        while (true) {
            checkAttackMinionWithMinion();
            checkAttackSpellWithMinion();
            checkAttackMinionWithHero();
            checkAttackHeroPowerWithMinion();
            checkAttackHeroPowerWithHero();
            checkAttackWeaponWithMinion();
            if (endGame())break;
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.println("Game ended!");
            }
        }
    }


    private void checkAttackMinionWithMinion() {
        if (map2.getMinionWantAttack() != null && map1.getMinionWantAttack() != null) {
            minion1 = map1.getMinionWantAttack();
            minion2 = map2.getMinionWantAttack();
            Log.body(gamePlayer1.getPlayer(), "attack started.", map1.getMinionWantAttack().getName() + " with " + map2.getMinionWantAttack().getName());
            Log.body(gamePlayer2.getPlayer(), "attack started.", map1.getMinionWantAttack().getName() + " with " + map2.getMinionWantAttack().getName());
            MinionAttack();
        }
    }

    public void MinionAttack() {
        if (mainMapper.MinionAttack(minion1, minion2)) {
            gamePlayer1.minionAttack();
            gamePlayer1.enemyMinionAttack();
            gamePlayer2.minionAttack();
            gamePlayer2.enemyMinionAttack();
        }
        map1.endClickLandCard();
        map2.endClickLandCard();
    }

    private void checkAttackSpellWithMinion() {
        if ((map1.getMinionWantAttack() != null && map2.getSpellWantAttack() != null)) {
            mainMapper.complexSpell1();
            map2.dealSpell(map2.getSpellWantAttack());
            map1.endClickLandCard();
            map2.changeUpdateFlagDeck();
            Log.body(gamePlayer1.getPlayer(), map2.getName() + ":spell attacked minion.", map2.getSpellWantAttack() + " & " + map1.getMinionWantAttack());
            Log.body(gamePlayer2.getPlayer(), map2.getName() + ":spell attacked minion.", map2.getSpellWantAttack() + " & " + map1.getMinionWantAttack());
        } else if ((map2.getMinionWantAttack() != null && map1.getSpellWantAttack() != null)) {
            mainMapper.complexSpell2();
            map1.dealSpell(map1.getSpellWantAttack());
            map2.endClickLandCard();
            map1.changeUpdateFlagDeck();
            Log.body(gamePlayer1.getPlayer(), map1.getName() + ":spell attacked minion.", map1.getSpellWantAttack() + " & " + map2.getMinionWantAttack());
            Log.body(gamePlayer2.getPlayer(), map1.getName() + ":spell attacked minion.", map1.getSpellWantAttack() + " & " + map2.getMinionWantAttack());

        }
    }

    private void checkAttackMinionWithHero() {
        if (map1.getMinionWantAttack() != null && map2.isHeroAttack()) {
            if (mainMapper.MinionAttackHero1()) {
                mainMapper.registerToRegister(map1.getGamePlayerLogic());
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
                gamePlayer2.updateHP(map1.getIndexMinionWantAttack(), map1);
                map1.endClickLandCard();
                gamePlayer2.updateHero(map2);
                gamePlayer1.updateHero(map2);
                map2.changeHeroAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":minion attacked hero.", map1.getSpellWantAttack() + " & " + map2.getHero());
                Log.body(gamePlayer2.getPlayer(), map1.getName() + ":minion attacked hero.", map1.getSpellWantAttack() + " & " + map2.getHero());
            }
        } else if (map2.getMinionWantAttack() != null && map1.isHeroAttack()) {
            if (mainMapper.MinionAttackHero2()) {
                mainMapper.registerToRegister(map2.getGamePlayerLogic());
                gamePlayer2.updateHP(map2.getIndexMinionWantAttack(), map2);
                gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
                map2.endClickLandCard();
                gamePlayer1.updateHero(map1);
                gamePlayer2.updateHero(map1);
                map1.changeHeroAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":minion attacked hero.", map2.getSpellWantAttack() + " & " + map1.getHero());
                Log.body(gamePlayer2.getPlayer(), map2.getName() + ":minion attacked hero.", map2.getSpellWantAttack() + " & " + map1.getHero());

            }
        }
    }

    private void checkAttackHeroPowerWithMinion() {
        if (map1.isHeroPowerWantAttack() && map2.getMinionWantAttack() != null) {
            if (map1.heroPowerAttackMinion(map2.getMinionWantAttack())) {
                gamePlayer2.updateHP(map2.getIndexMinionWantAttack(), map2);
                gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
                map2.endClickLandCard();
                map1.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":hero power attacked minion.", map1.getHero() + " & " + map2.getMinionWantAttack());
                Log.body(gamePlayer2.getPlayer(), map1.getName() + ":hero power attacked minion.", map1.getHero() + " & " + map2.getMinionWantAttack());
            }
        } else if (map2.isHeroPowerWantAttack() && map1.getMinionWantAttack() != null) {
            if (map2.heroPowerAttackMinion(map1.getMinionWantAttack())) {
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
                gamePlayer2.updateHP(map1.getIndexMinionWantAttack(), map1);
                map1.endClickLandCard();
                map2.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":hero power attacked minion.", map2.getHero() + " & " + map1.getMinionWantAttack());
                Log.body(gamePlayer2.getPlayer(), map2.getName() + ":hero power attacked minion.", map2.getHero() + " & " + map1.getMinionWantAttack());
            }
        }
    }

    private void checkAttackHeroPowerWithHero() {
        if (map1.isHeroPowerWantAttack() && map2.isHeroAttack()) {
            if (map1.heroPowerAttackHero(map2.getHero())) {
                gamePlayer2.updateHero(map2);
                gamePlayer1.updateHero(map2);
                map2.changeHeroAttack();
                map1.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":hero power attacked hero.", map1.getHero() + " & " + map2.getHero());
                Log.body(gamePlayer2.getPlayer(), map1.getName() + ":hero power attacked hero.", map1.getHero() + " & " + map2.getHero());
            }
        } else if (map2.isHeroPowerWantAttack() && map1.isHeroAttack()) {
            if (map2.heroPowerAttackHero(map1.getHero())) {
                gamePlayer1.updateHero(map1);
                gamePlayer2.updateHero(map1);
                map1.changeHeroAttack();
                map2.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":hero power attacked hero.", map2.getHero() + " & " + map1.getHero());
                Log.body(gamePlayer2.getPlayer(), map2.getName() + ":hero power attacked hero.", map2.getHero() + " & " + map1.getHero());
            }
        }
    }

    private void checkAttackWeaponWithMinion() {
        if (map1.getWeapon() != null && map1.isWeaponWantAttack() && map2.getMinionWantAttack() != null) {
            map1.getWeapon().weaponAttackMinion(map2.getMinionWantAttack());
            map2.endClickLandCard();
            map1.changeWeaponWantAttack();
            gamePlayer2.updateHP(map2.getIndexMinionWantAttack(), map2);
            gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
            gamePlayer1.updateWeapon(map1);
            gamePlayer2.updateWeapon(map1);
            Log.body(gamePlayer1.getPlayer(), map1.getName() + ":weapon attacked minion.", map1.getWeapon() + " & " + map2.getMinionWantAttack());
            Log.body(gamePlayer2.getPlayer(), map1.getName() + ":weapon attacked minion.", map1.getWeapon() + " & " + map2.getMinionWantAttack());
        } else if (map2.getWeapon() != null && map2.isWeaponWantAttack() && map1.getMinionWantAttack() != null) {
            map2.getWeapon().weaponAttackMinion(map1.getMinionWantAttack());
            map1.endClickLandCard();
            map2.changeWeaponWantAttack();
            gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
            gamePlayer2.updateHP(map1.getIndexMinionWantAttack(), map1);
            gamePlayer2.updateWeapon(map2);
            gamePlayer1.updateWeapon(map2);
            Log.body(gamePlayer1.getPlayer(), map2.getName() + ":weapon attacked minion.", map2.getWeapon() + " & " + map1.getMinionWantAttack());
            Log.body(gamePlayer2.getPlayer(), map2.getName() + ":weapon attacked minion.", map2.getWeapon() + " & " + map1.getMinionWantAttack());

        }
    }

    public boolean endGame() {
        if (map1.getDeck().getHero().getHP() < 1 || gamePlayer1.isExitInPlay() || endGame1) {
            gamePlayer2.setStop(true);
            gamePlayer1.setStop(true);
            looser(gamePlayer1);
            winner(gamePlayer2);
            gamePlayer2.endGame("winner");
            if (!gamePlayer1.isExitInPlay() && !endGame1)
                gamePlayer1.endGame("looser");
            MyFile.saveChanges(gamePlayer1.getPlayer());
            MyFile.saveChanges(gamePlayer2.getPlayer());
            gamePlayer1.setClientState("start");
            gamePlayer2.setClientState("start");
            gamePlayer1.setPLAY(null);
            gamePlayer2.setPLAY(null);
            return true;
        } else if (map2.getDeck().getHero().getHP() < 1 || gamePlayer2.isExitInPlay() || endGame2) {
            gamePlayer2.setStop(true);
            gamePlayer1.setStop(true);
            looser(gamePlayer2);
            winner(gamePlayer1);
            gamePlayer1.endGame("winner");
            if (!gamePlayer2.isExitInPlay() && !endGame2)
                gamePlayer2.endGame("looser");
            MyFile.saveChanges(gamePlayer2.getPlayer());
            MyFile.saveChanges(gamePlayer1.getPlayer());
            gamePlayer1.setClientState("start");
            gamePlayer2.setClientState("start");
            gamePlayer1.setPLAY(null);
            gamePlayer2.setPLAY(null);
            return true;
        }
        return false;
    }

    public void changeGamePlayer() {
        if (currentGamePlayer.getClientName().equals("player1")) {
            map1.changeTurn("player1");
            map2.changeTurn("player1");
            currentGamePlayer = gamePlayer2;
        } else {
            map1.changeTurn("player2");
            map2.changeTurn("player2");
            currentGamePlayer = gamePlayer1;
        }
    }

    private void looser(ClientHandler clientHandler) {
        Player player = clientHandler.getPlayer();
        int sumCups = 0;
        for (Deck deck : player.getDecks()) {
            if (deck != null) {
                sumCups += deck.getCup();
                if (deck.getName().equals(clientHandler.getMapperPlay().getDeck().getName())) {
                    deck.setCup(deck.getCup() - 1);
                    deck.setNumberGame(deck.getNumberGame() + 1);
                }
            }
        }
        if (sumCups > 0) {
            player.setCup(player.getCup() - 1);
        }
        int[] level = new int[10];
        level[0] = 0;
        for (int i = 1; i < 10; i++) {
            level[i] = player.getLevel()[i - 1];
        }
        player.setLevel(level);
    }

    private void winner(ClientHandler clientHandler) {
        Player player = clientHandler.getPlayer();
        for (Deck deck : player.getDecks()) {
            if (deck != null) {
                if (deck.getName().equals(clientHandler.getMapperPlay().getDeck().getName())) {
                    deck.setCup(deck.getCup() + 1);
                    deck.setNumberGame(deck.getNumberGame() + 1);
                    deck.setNumberWin(deck.getNumberWin() + 1);
                }
            }
        }
        player.setCup(player.getCup() + 1);
        int[] level = new int[10];
        level[0] = 1;
        for (int i = 1; i < 10; i++) {
            level[i] = player.getLevel()[i - 1];
        }
        player.setLevel(level);
    }

    public ClientHandler getGamePlayer1() {
        return gamePlayer1;
    }

    public ClientHandler getGamePlayer2() {
        return gamePlayer2;
    }

    public void setEndGame1(boolean endGame1) {
        this.endGame1 = endGame1;
    }

    public void setEndGame2(boolean endGame2) {
        this.endGame2 = endGame2;
    }
}
