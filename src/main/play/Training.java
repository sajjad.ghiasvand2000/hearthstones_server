package main.play;

import Entity.Deck;
import Entity.cards.minion.Minion;
import data.Log;
import main.ClientHandler;
import states.playState.MapperPlay;

public class Training extends Thread {
    private static MainMapper mainMapper;
    private ClientHandler gamePlayer1;
    private MapperPlay currentGamePlayer;
    private MapperPlay map1;
    private MapperPlay map2;
    private Minion minion1;
    private Minion minion2;

    public Training(ClientHandler gamePlayer1) {
        this.gamePlayer1 = gamePlayer1;
        map1 = gamePlayer1.getMapperPlay();
        map2 = new MapperPlay("PLAYER2", new Deck(map1.getPlayer().getDecks()[0]), map1.getPlayer());
        gamePlayer1.setEnemyMapperPlay(map2);
        MainMapper.setMainMapper(new MainMapper(map1.getGamePlayerLogic(), map2.getGamePlayerLogic()));
        mainMapper = MainMapper.getInstance();
        mainMapper.setFirstTurn();
        currentGamePlayer = map1;
    }

    @Override
    public void run() {
        while (true) {
            checkAttackMinionWithMinion();
            checkAttackSpellWithMinion();
            checkAttackMinionWithHero();
            checkAttackHeroPowerWithMinion();
            checkAttackHeroPowerWithHero();
            checkAttackWeaponWithMinion();
            endGame();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkAttackMinionWithMinion() {
        if (map2.getMinionWantAttack() != null && map1.getMinionWantAttack() != null) {
            minion1 = map1.getMinionWantAttack();
            minion2 = map2.getMinionWantAttack();
            Log.body(gamePlayer1.getPlayer(), "attack started.", map1.getMinionWantAttack().getName() + " with " + map2.getMinionWantAttack().getName());
            MinionAttack();
        }
    }

    public void MinionAttack() {
        if (mainMapper.MinionAttack(minion1, minion2)) {
            gamePlayer1.minionAttack();
            gamePlayer1.enemyMinionAttack();
        }
        map1.endClickLandCard();
        map2.endClickLandCard();
    }

    private void checkAttackSpellWithMinion() {
        if ((map1.getMinionWantAttack() != null && map2.getSpellWantAttack() != null)) {
            mainMapper.complexSpell1();
            map2.dealSpell(map2.getSpellWantAttack());
            map1.endClickLandCard();
            map2.changeUpdateFlagDeck();
            Log.body(gamePlayer1.getPlayer(), map2.getName() + ":spell attacked minion.", map2.getSpellWantAttack() + " & " + map1.getMinionWantAttack());
        } else if ((map2.getMinionWantAttack() != null && map1.getSpellWantAttack() != null)) {
            mainMapper.complexSpell2();
            map1.dealSpell(map1.getSpellWantAttack());
            map2.endClickLandCard();
            map1.changeUpdateFlagDeck();
            Log.body(gamePlayer1.getPlayer(), map1.getName() + ":spell attacked minion.", map1.getSpellWantAttack() + " & " + map2.getMinionWantAttack());
        }
    }

    private void checkAttackMinionWithHero() {
        if (map1.getMinionWantAttack() != null && map2.isHeroAttack()) {
            if (mainMapper.MinionAttackHero1()) {
                mainMapper.registerToRegister(map1.getGamePlayerLogic());
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
                map1.endClickLandCard();
                gamePlayer1.updateHero(map1);
                map2.changeHeroAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":minion attacked hero.", map1.getSpellWantAttack() + " & " + map2.getHero());
            }
        } else if (map2.getMinionWantAttack() != null && map1.isHeroAttack()) {
            if (mainMapper.MinionAttackHero2()) {
                mainMapper.registerToRegister(map2.getGamePlayerLogic());
                gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
                map2.endClickLandCard();
                gamePlayer1.updateHero(map1);
                map1.changeHeroAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":minion attacked hero.", map2.getSpellWantAttack() + " & " + map1.getHero());
            }
        }
    }

    private void checkAttackHeroPowerWithMinion() {
        if (map1.isHeroPowerWantAttack() && map2.getMinionWantAttack() != null) {
            if (map1.heroPowerAttackMinion(map2.getMinionWantAttack())) {
                gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
                map2.endClickLandCard();
                map1.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":hero power attacked minion.", map1.getHero() + " & " + map2.getMinionWantAttack());
            }
        } else if (map2.isHeroPowerWantAttack() && map1.getMinionWantAttack() != null) {
            if (map2.heroPowerAttackMinion(map1.getMinionWantAttack())) {
                gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
                map1.endClickLandCard();
                map2.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":hero power attacked minion.", map2.getHero() + " & " + map1.getMinionWantAttack());
            }
        }
    }

    private void checkAttackHeroPowerWithHero() {
        if (map1.isHeroPowerWantAttack() && map2.isHeroAttack()) {
            if (map1.heroPowerAttackHero(map2.getHero())) {
                gamePlayer1.updateHero(map2);
                map2.changeHeroAttack();
                map1.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map1.getName() + ":hero power attacked hero.", map1.getHero() + " & " + map2.getHero());
            }
        } else if (map2.isHeroPowerWantAttack() && map1.isHeroAttack()) {
            if (map2.heroPowerAttackHero(map1.getHero())) {
                gamePlayer1.updateHero(map1);
                map1.changeHeroAttack();
                map2.changeHeroPowerWantAttack();
                Log.body(gamePlayer1.getPlayer(), map2.getName() + ":hero power attacked hero.", map2.getHero() + " & " + map1.getHero());
            }
        }
    }

    private void checkAttackWeaponWithMinion() {
        if (map1.getWeapon() != null && map1.isWeaponWantAttack() && map2.getMinionWantAttack() != null) {
            map1.getWeapon().weaponAttackMinion(map2.getMinionWantAttack());
            map2.endClickLandCard();
            map1.changeWeaponWantAttack();
            gamePlayer1.updateHP(map2.getIndexMinionWantAttack(), map2);
            gamePlayer1.updateWeapon(map1);
            Log.body(gamePlayer1.getPlayer(), map1.getName() + ":weapon attacked minion.", map1.getWeapon() + " & " + map2.getMinionWantAttack());
        } else if (map2.getWeapon() != null && map2.isWeaponWantAttack() && map1.getMinionWantAttack() != null) {
            map2.getWeapon().weaponAttackMinion(map1.getMinionWantAttack());
            map1.endClickLandCard();
            map2.changeWeaponWantAttack();
            gamePlayer1.updateHP(map1.getIndexMinionWantAttack(), map1);
            gamePlayer1.updateWeapon(map2);
            Log.body(gamePlayer1.getPlayer(), map2.getName() + ":weapon attacked minion.", map2.getWeapon() + " & " + map1.getMinionWantAttack());
        }
    }

    private void endGame() {
        if (map1.getDeck().getHero().getHP() < 1 || map2.getDeck().getHero().getHP() < 1) {
            gamePlayer1.setStop(true);
            gamePlayer1.setClientState("start");
            gamePlayer1.endGame("finish");
        }
    }

    public void changeGamePlayer() {
        if (currentGamePlayer.getName().equals("player1")) {
            map1.changeTurn("player1");
            map2.changeTurn("player1");
            currentGamePlayer = map2;
        } else {
            map1.changeTurn("PLAYER2");
            map2.changeTurn("PLAYER2");
            currentGamePlayer = map1;
        }
    }

    public ClientHandler getGamePlayer1() {
        return gamePlayer1;
    }
}
