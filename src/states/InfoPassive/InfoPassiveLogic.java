package states.InfoPassive;

public class InfoPassiveLogic {

    private boolean twiceDrawPassive = false;
    private int offCardPassive = 0;
    private int ManaJumpPassive = 0;
    private int freePower = 0;
    private boolean nurse;

    public boolean isTwiceDrawPassive() {
        return twiceDrawPassive;
    }

    public void setTwiceDrawPassive(boolean twiceDrawPassive) {
        this.twiceDrawPassive = twiceDrawPassive;
    }

    public int getOffCardPassive() {
        return offCardPassive;
    }

    public void setOffCardPassive(int offCardPassive) {
        this.offCardPassive = offCardPassive;
    }

    public int getManaJumpPassive() {
        return ManaJumpPassive;
    }

    public void setManaJumpPassive(int manaJumpPassive) {
        ManaJumpPassive = manaJumpPassive;
    }

    public int getFreePower() {
        return freePower;
    }

    public void setFreePower(int freePower) {
        this.freePower = freePower;
    }

    public boolean isNurse() {
        return nurse;
    }

    public void setNurse(boolean nurse) {
        this.nurse = nurse;
    }
}
