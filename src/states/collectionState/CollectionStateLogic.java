package states.collectionState;

import Entity.Deck;
import Entity.MainPlayer;
import Entity.Player;

public class CollectionStateLogic {

    private Deck playDeck;
    private Player player;

    public CollectionStateLogic(Player player) {
        this.player = player;
        if (player.getDecks()[0] != null)
            playDeck = new Deck(player.getDecks()[0]);
    }

    public Deck getPlayDeck() {
        return playDeck;
    }

    public void setPlayDeck(Deck playDeck) {
        this.playDeck = playDeck;
    }
}
