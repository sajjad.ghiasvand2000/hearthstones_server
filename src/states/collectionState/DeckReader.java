package states.collectionState;

import Entity.Deck;
import Entity.EntityUtils;
import Entity.MainPlayer;
import Entity.cards.Card;
import Entity.hero.HeroClass;
import com.google.gson.Gson;
import data.MyFile;
import main.Util;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class DeckReader {
    private ArrayList<String> friend;
    private ArrayList<String> enemy;
    private Deck deck1;
    private Deck deck2;


    public DeckReader() {
        deck1 = new Deck();
        deck2 = new Deck();
        friend = new ArrayList<>();
        enemy = new ArrayList<>();
        convert();
        setCards();
        setHero();
    }

    private void convert() {
        Scanner fileReader = null;
        try {
            fileReader = new Scanner(new FileReader("deckReader.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Gson gson = new Gson();
        Map map = gson.fromJson(fileReader.nextLine(), Map.class);
        friend = (ArrayList) map.get("friend");
        enemy = (ArrayList) map.get("enemy");
        fileReader.close();
    }

    public void setCards() {
        List<Card> cards1 = new ArrayList<>();
        List<Card> cards2 = new ArrayList<>();
        for (String s : friend) {
            cards1.add(Util.findCardFromMainPlayer(s));
        }
        for (String s : enemy) {
            if (s.contains("reward")) {
                EntityUtils.setReward(s.substring(28));
                s = s.substring(0, 19);
            }
            cards2.add(Util.findCardFromMainPlayer(s));
        }
        deck1.setCards(cards1);
        deck2.setCards(cards2);
    }

    private void setHero() {
        deck1.setHero(MainPlayer.getInstance().getHeroes().get(0));
        deck2.setHero(MainPlayer.getInstance().getHeroes().get(0));
    }

    public Deck getDeck1() {
        return deck1;
    }

    public Deck getDeck2() {
        return deck2;
    }

//    public void write(){
//        try {
//            MyFile.fileWriter("deckReader.txt", makeDeckReader());
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    private DeckReader makeDeckReader(){
//        DeckReader deckReader = new DeckReader();
//        Collections.addAll(deckReader.friend, "Learn Draconic","book of specters","minion","minion","minion",
//                "spell","spell","spell","friendly smith","curio collector","pharaoh's Blessing","Polymorph","sprint",
//                "special hero card","special hero card","minion","minion","minion","minion","minion","minion","minion","minion" );
//        Collections.addAll(deckReader.enemy, "Strength in Numbers->reward: Security Rover","Dreadscale","weapon"
//                ,"swarm of locusts","tomb warden","sathrovarr","minion","minion","minion","minion","minion","Security Rover");
//        return deckReader;
//    }
}
