package states.playState.logic;

import Entity.Deck;
import Entity.EntityUtils;
import Entity.Player;
import Entity.cards.Card;
import Entity.cards.Type;
import Entity.cards.minion.EndTurn;
import Entity.cards.minion.Minion;
import Entity.cards.spell.QuestReward;
import Entity.cards.weapon.Deathrattle;
import Entity.hero.HeroClass;
import data.Log;
import main.Util;
import main.play.PlayStateLogic;
import states.InfoPassive.InfoPassiveLogic;
import java.util.ArrayList;
import java.util.Collections;

public class GamePlayerLogic {
    private Deck deck;
    private Player player;
    private ArrayList<Card> containDeckCard;
    private Minion[] containLandCard;
    private ArrayList<String> event;
    private int offCardsPassive;
    private int ManaJumpPassive;
    private boolean twiceDrawPassive;
    private int freePowerPassive;
    private boolean nurse;
    private int mana;
    private int level = 1;
    private int deckIndex = 3;
    private String name;
    private boolean turn = false;
    private int updateFlagACardInLand = -1;
    private boolean updateFlagDeck = false;
    private boolean specialUpdateFlag = false;
    private boolean heroUpdateFlag = false;
    private boolean updateWeaponFlag;
    private Card extraCard;
    private int numberOfCards;
    private SpellLogic spellLogic;
    private HeroLogic heroLogic;
    private MinionLogic minionLogic;
    private WeaponLogic weaponLogic;
    private String errorMassage = "";
    private boolean error = false;
    private QuestReward questReward;
    private InfoPassiveLogic infoPassiveLogic;

    public GamePlayerLogic(String name, Deck deck, Player player) {
        this.name = name;
        this.deck = deck;
        this.player = player;
        containDeckCard = new ArrayList<>();
        containLandCard = new Minion[7];
        infoPassiveLogic = new InfoPassiveLogic();
        initInfoPassive();
        numberOfCards = deck.getCards().size() - 3;
        mana = 1 + ManaJumpPassive;
        event = new ArrayList<>();
        Collections.shuffle(deck.getCards());
        Collections.addAll(containDeckCard, deck.getCards().get(0), deck.getCards().get(1), deck.getCards().get(2));
        spellLogic = new SpellLogic(this);
        heroLogic = new HeroLogic(this);
        minionLogic = new MinionLogic(this);
        weaponLogic = new WeaponLogic(this);
    }

    private void initInfoPassive() {
        offCardsPassive = infoPassiveLogic.getOffCardPassive();
        ManaJumpPassive = infoPassiveLogic.getManaJumpPassive();
        twiceDrawPassive = infoPassiveLogic.isTwiceDrawPassive();
        freePowerPassive = infoPassiveLogic.getFreePower();
        nurse = infoPassiveLogic.isNurse();
    }

    public void clickDeckCard(Card card) {
        minionLogic.clickOnMinion(card);
        spellLogic.clickOnSpell(card);
        weaponLogic.clickOnWeapon(card);
    }

    public boolean checkMana(Card card) {
        return mana >= card.getMana() - offCardsPassive;
    }

    public void decreaseMana(Card card) {
        mana -= card.getMana() - offCardsPassive;
        minionLogic.setMinionQuest(minionLogic.getMinionQuest() + card.getMana() - offCardsPassive);
        spellLogic.setSpellQuest(spellLogic.getSpellQuest() + card.getMana() - offCardsPassive);
    }

    public Card finedCardByRef(Card card) {
        for (Card card1 : containDeckCard) {
            if (card1.getName().equals(card.getName())) {
                Log.body(player, "A card was founded in containDeckCard like " + card.getName(), card1.getName());
                return card1;
            }
        }
        Log.body(player, "Any card was not founded in containDeckCard like " + card.getName(), "");
        return null;
    }

    public void changeUpdateFlagACardInLand() {
        updateFlagACardInLand = -1;
    }

    public void changeSpecialUpdateFlag() {
        specialUpdateFlag = !specialUpdateFlag;
    }

    public void changeUpdateFlagDeck() {
        updateFlagDeck = !updateFlagDeck;
    }

    public void makeTrueUpdateFlagDeck(){
        updateFlagDeck = true;
    }

    public void makeFalseUpdateFlagDeck(){
        updateFlagDeck = false;
    }

    public void changeExtraCard() {
        extraCard = null;
    }

    public void changeHeroUpdateFlag() {
        heroUpdateFlag = !heroUpdateFlag;
    }

    public void changeUpdateWeaponFlag() {
        updateWeaponFlag = !updateWeaponFlag;
    }

    public void changeQuestReward() {
        questReward = null;
    }

    public synchronized void setOneContainLandCard(Card minion, int i) {
        Log.body(player, name + ":set a card in land", minion.getName());
        setContainLandCard((Minion) minion, i);
        containLandCard[i].setRush(true);
        containLandCard[i].setCharge(true);
        updateFlagACardInLand = i;
    }

    public void setContainLandCard(Minion minion, int i) {
        containLandCard[i] = minion;
        if (minion.isUpdate()) {
            updateFlagACardInLand = i;
        }
    }

    public void checkHPLandCards(int i) {
        minionLogic.checkShotbot(i);
        if (containLandCard[i] != null) {
            if (containLandCard[i].getHP() < 1) {
                containLandCard[i] = null;
            }
        }
    }

    public void checkDurability() {
        if (weaponLogic.getWeapon() != null) {
            if (weaponLogic.getWeapon().getDurability() < 1) {
                if (weaponLogic.getWeapon().getDescription().contains("Deathrattle")) {
                    ((Deathrattle) weaponLogic.getWeapon()).deathrattle(this);
                }
                weaponLogic.setWeapon(null);
            }
        }
    }

    public void makeALandCardNull(int i) {
        containLandCard[i] = null;
    }

    public void clickEndTurn() {
        level++;
        mana = Math.min(level, 10);
        PlayStateLogic.setWhichMinionWantAttack(null);
        minionLogic.setMinionWantSummon(null);
        minionLogic.setMinionWantAttack(null);
        spellLogic.setSpellWantAttack(null);
        heroLogic.setHeroAttack(false);
        minionLogic.setRegister1(new Minion[7]);
        minionLogic.setRegister2(new Minion[7]);
        callEndTurnMinion();
        nurse();
        if (deck.getHero().getHeroClass().equals(HeroClass.Paladin))
            EntityUtils.addAMinionHPAndAttack(this);
        heroLogic.setCanUseHeroPower(true);
        heroLogic.setHowManyHeroPower(0);
        weaponLogic.setCanUseWeapon(true);
        checkWindFury();
    }

    private void callEndTurnMinion() {
        for (Minion minion : containLandCard) {
            if (minion != null) {
                if (minion.getDescription().contains("end")) {
                    ((EndTurn) minion).endTurn(this);
                    Log.body(player, name + ":a end turn minion called.", minion.getName());
                }
            }
        }
    }

    private void nurse() {
        if (nurse) {
            int x = EntityUtils.finedNonZeroRandomIndex(this);
            if (x != -1) {
                Card card = Util.findCardFromMainPlayer(containLandCard[x].getName());
                containLandCard[x].setHP(((Minion) card).getHP());
                updateFlagACardInLand = x;
                Log.body(player, name + "nurse passive accrued.", "");
            }
        }
    }

    private void checkWindFury() {
        for (Minion minion : containLandCard) {
            if (minion != null) {
                if (minion.isWindFury())
                    minion.setNumberWinFury(0);
            }
        }
    }

    public void addACardToDeck(Card card) {
        if (containDeckCard.size() < 12) {
            containDeckCard.add(card);
            extraCard = card;
        }
    }

    public void addCardToHand() {
        if (deckIndex < deck.getCards().size() && containDeckCard.size() < 12) {
            containDeckCard.add(deck.getCards().get(deckIndex));
            checkCurioCollector(deck.getCards().get(deckIndex));
            if (twiceDrawPassive) {
                if (deckIndex + 1 < deck.getCards().size() && containDeckCard.size() < 12) {
                    containDeckCard.add(deck.getCards().get(deckIndex + 1));
                    deckIndex++;
                    numberOfCards--;
                }
            }
        } else {
            errorMassage = "Your hand if full!";
        }
        if (numberOfCards != 0)
            numberOfCards--;
        deckIndex++;
    }

    public void checkCurioCollector(Card card) {
        if (card.getType().equals(Type.MINION)) {
            minionLogic.checkCurioCollector((Minion) card);
        }
    }

    public void changeTurn(String name) {
        turn = !name.equals(this.name);
    }

    public void changeError() {
        error = !error;
    }

    public void changeErrorMassage() {
        errorMassage = "";
    }

    public void setErrorMassage(String massage) {
        error = true;
        errorMassage = massage;
    }

    public Deck getDeck() {
        return deck;
    }

    public ArrayList<Card> getContainDeckCard() {
        return containDeckCard;
    }

    public ArrayList<String> getEvent() {
        return event;
    }

    public int getMana() {
        return mana;
    }

    public int getLevel() {
        return level;
    }

    public int getDeckIndex() {
        return deckIndex;
    }

    public Minion[] getContainLandCard() {
        return containLandCard;
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public int getUpdateFlagACardInLand() {
        return updateFlagACardInLand;
    }

    public String getName() {
        return name;
    }

    public boolean isSpecialUpdateFlag() {
        return specialUpdateFlag;
    }

    public void setUpdateFlagACardInLand(int updateFlag) {
        this.updateFlagACardInLand = updateFlag;
    }

    public boolean isUpdateFlagDeck() {
        return updateFlagDeck;
    }

    public Card getExtraCard() {
        return extraCard;
    }

    public void setExtraCard(Card extraCard) {
        this.extraCard = extraCard;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public int getNumberOfCards() {
        return numberOfCards;
    }

    public void setNumberOfCards(int numberOfCards) {
        this.numberOfCards = numberOfCards;
    }

    public void setDeckIndex(int deckIndex) {
        this.deckIndex = deckIndex;
    }

    public int getOffCardsPassive() {
        return offCardsPassive;
    }

    public SpellLogic getSpellLogic() {
        return spellLogic;
    }

    public HeroLogic getHeroLogic() {
        return heroLogic;
    }

    public boolean isHeroUpdateFlag() {
        return heroUpdateFlag;
    }

    public MinionLogic getMinionLogic() {
        return minionLogic;
    }

    public String getErrorMassage() {
        return errorMassage;
    }

    public boolean isError() {
        return error;
    }

    public int getFreePowerPassive() {
        return freePowerPassive;
    }

    public WeaponLogic getWeaponLogic() {
        return weaponLogic;
    }

    public boolean isUpdateWeaponFlag() {
        return updateWeaponFlag;
    }

    public QuestReward getQuestReward() {
        return questReward;
    }

    public void setQuestReward(QuestReward questReward) {
        this.questReward = questReward;
    }

    public InfoPassiveLogic getInfoPassiveLogic() {
        return infoPassiveLogic;
    }

    public Player getPlayer() {
        return player;
    }
}
