package states.rankState;

import Entity.Player;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;

public class RankLogic {
    private Player player;
    private ArrayList<String> ranked;

    public RankLogic(Player player) {
        this.player = player;
        ranked = rankedList();
    }

    private ArrayList<String> rankedList() {
        ArrayList<String> list = new ArrayList<>();
        Scanner fileReader = null;
        try {
            fileReader = new Scanner(new FileReader("player.txt"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        while (fileReader.hasNext()) {
            Gson gson = new Gson();
            Map map = gson.fromJson(fileReader.nextLine(), Map.class);
            Double cup = (Double) map.get("cup");
            int x = cup.intValue();
            list.add((String) map.get("username") + "_" + x);
        }
        fileReader.close();
        Collections.sort(list, (s, t1) -> Integer.compare(Integer.parseInt(t1.split("_")[1]), Integer.parseInt(s.split("_")[1])));
        return list;
    }

    public String[] findTopTenScores() throws IOException {
        String[] topTen = new String[10];
        for (int i = 0; i < 10; i++) {
            topTen[i] = ranked.get(i);
        }
        return topTen;
    }

    public String[] findMe() {
        String[] me = new String[11];
        int index = 0;
        for (int i = 0; i < ranked.size(); i++) {
            if (ranked.get(i).split("_")[0].equals(player.getUsername())) {
                index = i;
                me[5] = (index + 1) + "_" + ranked.get(i);
            }

        }
        for (int i = 0; i < Math.min(ranked.size() - index - 1, 5); i++) {
            me[6 + i] = (index + 2 + i) + "_" + ranked.get(index + i + 1);
        }
        for (int i = 0; i > -1 * Math.min(index, 5); i--) {
            me[4 + i] = (index + i) + "_" + ranked.get(index + i - 1);
        }
        return me;
    }


}
