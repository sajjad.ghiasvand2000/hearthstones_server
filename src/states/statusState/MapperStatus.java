package states.statusState;

import Entity.Deck;
import Entity.Player;
import states.statusState.logic.StatusLogic;

import java.util.List;

public class MapperStatus {
    private StatusLogic statusLogic;

    public MapperStatus(Player player){
        statusLogic = new StatusLogic(player);
    }

    public List<Deck> getComparedDecks(){
        return statusLogic.getContainComparedDecks();
    }
}
