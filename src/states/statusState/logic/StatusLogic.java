package states.statusState.logic;

import Entity.Deck;
import Entity.MainPlayer;
import Entity.Player;
import Entity.cards.Card;
import java.util.ArrayList;
import java.util.List;

public class StatusLogic {

    private DeckComparator deckComparator;
    private List<Deck> containComparedDecks;
    private Player player;

    public StatusLogic(Player player){
        this.player = player;
        containComparedDecks = changeMainPlayerDeckToList();
        deckComparator = new DeckComparator();
        if (player.getDecks()[0] != null)
            containComparedDecks.sort(deckComparator);
    }

    public static float meanOfMamaDeck(Deck deck){
        float sum = 0;
        float number = deck.getCards().size();
        for (Card card : deck.getCards()) {
            sum += card.getMana();
        }
        return sum/number;
    }

    public static String mostPlayedCard(Deck deck){
        Card maxCard = deck.getCards().get(0);
        int max = 0;
        for (Card card : deck.getCards()) {
            if (card.getNumberDraw() > max){
                max = card.getNumberDraw();
                maxCard = card;
            }
        }
        return maxCard.getName();
    }

    private List<Deck> changeMainPlayerDeckToList(){
        List<Deck> list = new ArrayList<>();
        for (Deck deck : player.getDecks()) {
            if (deck != null)
                list.add(deck);
        }
        return list;
    }

    public List<Deck> getContainComparedDecks() {
        return containComparedDecks;
    }
}
